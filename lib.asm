section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		mov rsi, [rdi + rax]
		and rsi, 0xFF
		test rsi, rsi
		jz .quit
		inc rax
		jmp .loop
	.quit:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	mov rdx, rax
	mov rax, 1
	pop rsi
	mov rdi, 1
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, '\n'

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdx, 1
	mov rax, 1
	mov rsi, rsp
	mov rdi, 1
	syscall
	pop rdi
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rbp
	mov rbp, rsp
	sub rsp, 56
	mov rax, rdi
	mov rcx, 10
	dec rbp
	mov byte[rbp], 0
	.loop:
		xor rdx, rdx
		div rcx
		add dl, '0'
		dec rbp
		mov byte[rbp], dl
		test rax, rax
		jnz .loop
	mov rdi, rbp
	call print_string
	add rsp, 56
	pop rbp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jge print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	.loop:
		mov r10b, byte[rdi]
		mov r11b, byte[rsi]
		inc rdi
		inc rsi
		cmp r10b, r11b
		jnz .false
		test r10b, r10b      
		jnz .loop
		mov rax, 1
		ret
	.false:
		xor rax, rax
		ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rdx, 1
	mov rsi, rsp
	xor rdi, rdi
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	dec rsi
	push r12
	mov r12, rsi
	push r13
	mov r13, rdi
	push r14
	xor r14, r14
	.loop:
		cmp r14, r12
		jg .out_of_buffer
		call read_char
		mov [r13 + r14], al
		test al, al
		jz .quit
		inc r14
		jmp .loop
	.quit:
		mov byte[r13 + r14], 0
		mov rdx, r14
		mov rax, r13
		pop r14
		pop r13
		pop r12
		ret
	.out_of_buffer:
		xor rax, rax
		pop r14
		pop r13
		pop r12
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor rcx, rcx
	.loop:
		mov	cl, byte[rdi + rdx]
		test cl, cl
		jz .quit
		sub cl, '0'	
		test cl, cl
		jl .quit
		cmp	cl, 9
		ja .quit
		imul rax, 10
		add al, cl
		inc rdx
		jmp .loop
	.quit:
		ret


; Принимает указатель на строку, пытается
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, [rdi]
	cmp al, '-'
	jz .minus
	call parse_uint
	ret
	.minus:
		inc rdi
		call parse_uint
		inc rdx
		neg rax
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r10, r10
	.loop:
		cmp rax, rdx
		jge .out_of_buffer
		mov r10b, byte[rdi + rax]
		mov byte[rsi + rax], r10b
		test r10b, r10b
		jz .quit
		inc rax
		jmp .loop
	.out_of_buffer:
		xor rax, rax
	.quit:
		ret
