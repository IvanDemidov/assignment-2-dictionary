%include "lib.inc"

section .text

global find_word
find_word:
    test rsi, rsi
    jz .fin
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    .read_str:
        mov r9b, byte[rsi]
        test r9b, r9b
        jnz .not_zero
    inc rsi
    test rax, rax
    jz .not_equals
    add rsi, 8
    mov rax, rsi
    ret
    .not_equals:
        mov rsi, [rsi]
        jmp find_word
    .not_zero:
        inc rsi
        jmp .read_str
    .fin:
        xor rax, rax
        ret
