%include "words.inc"

%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256
%define SYS_WRITE 1
%define STD_ERR 2
%define ERROR_NOT_FOUND_MESSAGE 'String not found'
%define ERROR_TOO_LARGE_MESSAGE 'String too large'
%strlen ERROR_NOT_FOUND_MESSAGE_SIZE ERROR_NOT_FOUND_MESSAGE
%strlen ERROR_TOO_LARGE_MESSAGE_SIZE ERROR_TOO_LARGE_MESSAGE
%define ERROR_END_CODE 1

section .rodata
    error_string_too_large: db ERROR_TOO_LARGE_MESSAGE
    error_string_not_found: db ERROR_NOT_FOUND_MESSAGE

section .bss
    text: resb BUFFER_SIZE

section .text

global _start
_start:
    mov rsi, BUFFER_SIZE
    mov rdi, text
    call read_word
    mov rdi, error_string_too_large
    mov rdx, ERROR_TOO_LARGE_MESSAGE_SIZE
    test rax, rax
    jz .error
    mov rdi, rax
    mov rsi, first_word
    call find_word
    mov rdi, error_string_not_found
    mov rdx, ERROR_NOT_FOUND_MESSAGE_SIZE
    test rax, rax
    jz .error
    mov rdi, rax
    call print_string
    xor rdi, rdi
    jmp exit
    .error:
        mov     rsi, rdi
        mov     rax, SYS_WRITE
        mov     rdi, STD_ERR
        syscall
        mov rdi, ERROR_END_CODE
        jmp exit
