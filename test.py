import subprocess
import os

path = "./main"

class FailedTest:
    def __init__(self, test):
        self.test = test
    def setOutput(self, programOutput):
        self.programOutput = programOutput
        return self
    def setError(self, programError):
        self.programError = programError
        return self

class Test:
    def __init__(self, num):
        self.correctError = ""
        self.correctOutput = ""
        self.returnCode = -1
        self.num = num
    def setInput(self, testInput):
        self.testInput = testInput
        return self
    def setCorrectOutput(self, correctOutput):
        self.correctOutput = correctOutput
        self.returnCode = 0
        return self
    def setCorrectError(self, correctError):
        self.correctError = correctError
        self.returnCode = 1
        return self

tests = [Test(1).setInput("first").setCorrectOutput("first-str"),
        Test(2).setInput("second").setCorrectOutput("second-str"),
        Test(3).setInput("third").setCorrectOutput("third-str"),
        Test(4).setInput("str").setCorrectError("String not found"),
        Test(5).setInput("").setCorrectError("String not found"),
        Test(6).setInput("Ivan Demidov").setCorrectOutput("fourth-str"),
        Test(7).setInput("D"*254).setCorrectError("String not found"),
        Test(8).setInput("D"*255).setCorrectError("String not found"),
        Test(9).setInput("D"*256).setCorrectError("String too large"),
        Test(10).setInput("D"*260).setCorrectError("String too large")]

failedTests = []

print("Running " + str(len(tests)) + " tests")
print("----------------------------------------")

for i in range(len(tests)):

    result = subprocess.Popen([path], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    programOutput, programError = result.communicate(input = tests[i].testInput.encode())
    returnCode = result.returncode

    programOutput = programOutput.decode().strip()
    programError = programError.decode().strip()

    if programOutput == tests[i].correctOutput and programError == tests[i].correctError and returnCode == tests[i].returnCode:
        print("|OK|", end="")
    else:
        print("|WA|", end="")
        failedTests.append(FailedTest(tests[i]).setOutput(programOutput).setError(programError))
print('\n')

if not failedTests:
    print("\nOK")

for currentFailedTest in failedTests:
    print("----------------------------")

    
    print("The solution broke down on the test number" + str(currentFailedTest.test.num) + ". Input: " + currentFailedTest.test.testInput + "")

    print("Have to be:")
    if (currentFailedTest.test.correctOutput != ""):
        print("Stdout: ", "\"" + currentFailedTest.test.correctOutput + "\"")
    if (currentFailedTest.test.correctError != ""):
        print("Stdout: ", "\"" + currentFailedTest.test.correctError + "\"")

    print("Program shew:")

    if (currentFailedTest.programOutput != ""):
        print("Stdout: ", "\"" + currentFailedTest.programOutput + "\"")
    if (currentFailedTest.programError != ""):
        print("Stdout: ", "\"" + currentFailedTest.programError + "\"")
